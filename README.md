# SAIDA - Situational Awareness and Intelligent Decision Automation

_SAIDA - Situational Awareness and Intelligent Decision Automation_ is a new research project that seeks to integrate a full range of Artificial Intelligence (AI) technologies, including Machine Learning (ML), Natural Language Processing (NLP), Knowledge Graph (KG), Ontologies, Fuzzy Rules Systems, Semantic Reasoning, and Multi-Agent Systems (MAS), to orchestrate decisional intelligence by humans and machines, and automate cybersecurity response capabilities.

## Definitions

_Cybersecurity Situational Awareness (CyberSA)_ guides teamwork in Cyber Incident Responses, Control, Containment, and Countermeasures (CIRC3). It has become a reliable methodology to support CyberOps in more complex environments keen on rapid learning and perfecting capabilities. Its effectiveness depends heavily on the quality of cybersecurity protocols, and their fast-paced dynamic integration through strategically coordinated actions within and among incident teams.

_Intelligent Decision Automation (IDA)_ is a generic capability in Information Systems (IS) research, building upon decades of research on AI, Expert Systems, and Decision Support Systems. It seeks to integrate the full range of AI functionality to formalize and enable automated execution of Knowledge Representation and Logic (KRL), rules-based decision models, and contextual awareness. Cybersecurity is a prime candidate for IDA given its increasing complexity, the knowledge integration and teamwork nature of CIRC3, the importance of Human-Computer Interaction (HCI), and the feasibility to decompose cybersecurity rules to address threats at the most micro levels fully integrated to all other levels and to the macro context, whether tactical or strategic.

## Objectives
TBC

## Installation
TBC

## Usage
TBC

## Support
TBC

## Roadmap
TBC

## Contributing
TBC

## Authors and acknowledgment
TBC

## License
TBC

## Project status
TBC
